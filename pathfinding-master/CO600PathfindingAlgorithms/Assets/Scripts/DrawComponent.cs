﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawComponent : MonoBehaviour
{
    public MapGenerator map;
    public PathFindingResults results;
    public float visitedTilesPerSecond;
    public float pathTilesPerSecond;
    public Color visitedTileColor;
    public Color pathColor;

    float timer = 0.0f;
    int visitedDrawStep = 0;
    int pathDrawStep = 0;

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (results != null)
        {
            while (timer >= 1.0f / visitedTilesPerSecond && visitedDrawStep < results.visited.Count)
            {
                MapGenerator.Coord position = results.visited[visitedDrawStep];
                timer -= 1.0f / visitedTilesPerSecond;
                Renderer cubeRenderer = map.cubes[position.tileX, position.tileY].GetComponent<Renderer>();
                cubeRenderer.material.SetColor("_Color", visitedTileColor);
                visitedDrawStep++;
            }
            if (visitedDrawStep == results.visited.Count)
            {
                while (timer >= 1.0f / pathTilesPerSecond && pathDrawStep < results.path.Count)
                {
                    MapGenerator.Coord position = results.path[pathDrawStep];
                    timer -= 1.0f / pathTilesPerSecond;
                    Renderer cubeRenderer = map.cubes[position.tileX, position.tileY].GetComponent<Renderer>();
                    cubeRenderer.material.SetColor("_Color", pathColor);
                    ++pathDrawStep;
                }
            }
            if(pathDrawStep == results.path.Count)
            {
                Destroy(this);
            }
        }
    }
}
