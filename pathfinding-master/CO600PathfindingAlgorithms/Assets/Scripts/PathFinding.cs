﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class PathFinding : MonoBehaviour
{
    public Graph graph = new Graph();

    PathFindingResults results;

    //graph for the dfs
    public class Graph
    {
        Dictionary<MapGenerator.Coord, List<MapGenerator.Coord>> adjacencyList;

        public Graph()
        {
            adjacencyList = new Dictionary<MapGenerator.Coord, List<MapGenerator.Coord>>();
        }


        /*
         * Adds a vertex if it doesn't already exist in the graph.
         */
        private void TryInitVertex(MapGenerator.Coord position)
        {
            if (!adjacencyList.ContainsKey(position))
            {
                adjacencyList.Add(position, new List<MapGenerator.Coord>());
            }
        }

        /*
         * Adds an edge betwen two points.
         */
        public void AddEdge(MapGenerator.Coord from, MapGenerator.Coord to)
        {
            TryInitVertex(from);
            TryInitVertex(to);

            var neighbours = adjacencyList[from];
            if (!neighbours.Contains(to))
            {
                neighbours.Add(to);
            }
        }

        /*
         * Builds a graph of the traversable area of the map.
         */
        public void BuildGraph(MapGenerator map)
        {
            adjacencyList.Clear();

            for (int x = 0; x < map.width; x++)
            {
                for (int y = 0; y < map.height; y++)
                {
                    if (map.map[x, y] == 0)
                    {
                        for (int i = -1; i <= 1; i++)
                        {
                            for (int j = -1; j <= 1; j++)
                            {
                                if (map.IsInMapRange(x + i, y + j) && map.map[x + i, y + j] == 0 && (i != 0 || j != 0))
                                {
                                    // Add all valid edges and their neighbours to the graph.
                                    AddEdge(new MapGenerator.Coord(x, y), new MapGenerator.Coord(x + i, y + j));
                                }
                            }
                        }
                    }
                }
            }
        }

        /*
         * Runs the depth first algorithm on the graph.
         */
        public PathFindingResults DepthFirst(MapGenerator.Coord start, MapGenerator.Coord dest)
        {
            PathFindingResults results = new PathFindingResults();
            Stack<MapGenerator.Coord> stack = new Stack<MapGenerator.Coord>();
            HashSet<MapGenerator.Coord> visited = new HashSet<MapGenerator.Coord>();


            if (!adjacencyList.ContainsKey(start) || !adjacencyList.ContainsKey(dest))
            {
                // if the adjacency list does not contain the start point or the destination return early
                return results;
            }
            stack.Push(start);
            while (stack.Count > 0)
            {
                MapGenerator.Coord current = stack.Pop();
                if (current.tileX == dest.tileX && current.tileY == dest.tileY)
                {
                    // If we find the destination we want to end the algorithm early to save resources.
                    break;
                }
                if (!visited.Contains(current))
                {
                    visited.Add(current); // marks coordinate as having been visited.
                    results.visited.Add(current); // for visualisation
                    foreach (MapGenerator.Coord neighbour in adjacencyList[current])
                    {
                        stack.Push(neighbour);
                    }
                }
            }

            foreach (MapGenerator.Coord position in stack)
            {
                if (visited.Contains(position))
                {
                    results.path.Add(position);
                }
            }
            results.path.Reverse();
            return results;
        }

        public PathFindingResults BreadthFirst(MapGenerator.Coord start, MapGenerator.Coord dest)
        {
            PathFindingResults results = new PathFindingResults();
            Queue<MapGenerator.Coord> queue = new Queue<MapGenerator.Coord>();
            HashSet<MapGenerator.Coord> visited = new HashSet<MapGenerator.Coord>();
            Dictionary<MapGenerator.Coord, MapGenerator.Coord> parents = new Dictionary<MapGenerator.Coord, MapGenerator.Coord>();

            if (!adjacencyList.ContainsKey(start) || !adjacencyList.ContainsKey(dest))
            {
                return results;
            }
            queue.Enqueue(start);
            visited.Add(start);
            results.visited.Add(start); // for visualisation
            MapGenerator.Coord lastVertex = new MapGenerator.Coord(0, 0);

            while (queue.Count > 0)
            {
                MapGenerator.Coord current = queue.Dequeue();
                if (current.tileX == dest.tileX && current.tileY == dest.tileY)
                {
                    lastVertex = current;
                    break;
                }
                foreach (MapGenerator.Coord neighbour in adjacencyList[current])
                {
                    if (!visited.Contains(neighbour))
                    {
                        queue.Enqueue(neighbour);
                        parents[neighbour] = current;
                        visited.Add(neighbour);
                        results.visited.Add(neighbour); // for visualisation
                    }
                }
            }

            /*
             * Recover the path by following the vertices back from the destination. (Like breadcrumbs.)
             */

            MapGenerator.Coord keyVertex = lastVertex;

            results.path.Add(keyVertex);

            while (parents.TryGetValue(keyVertex, out MapGenerator.Coord parentVertex))
            {
                results.path.Add(parentVertex);
                keyVertex = parentVertex;
            }
            results.path.Reverse();

            return results;
        }

        public PathFindingResults Dijkstra(MapGenerator.Coord start, MapGenerator.Coord dest)
        {
            PathFindingResults results = new PathFindingResults();
            Dictionary<MapGenerator.Coord, float> distance = new Dictionary<MapGenerator.Coord, float>();
            Dictionary<MapGenerator.Coord, MapGenerator.Coord> previous = new Dictionary<MapGenerator.Coord, MapGenerator.Coord>();
            Heap heap = new Heap();


            foreach (MapGenerator.Coord vertex in adjacencyList.Keys)
            {
                float distanceValue = vertex != start ? float.PositiveInfinity : 0;
                distance.Add(vertex, distanceValue);
                heap.Insert(vertex, distanceValue);
            }

            MapGenerator.Coord lastVertex = start;

            while (heap.Count > 0)
            {
                var u = heap.TakeMin();
                lastVertex = u.position;

                results.visited.Add(u.position); // for visualisation of visited nodes.

                if (u.position == dest)
                {
                    break;
                }

                foreach (MapGenerator.Coord neighbour in adjacencyList[u.position])
                {
                    if (heap.Contains(neighbour))
                    {
                        float alt = u.distance + MapGenerator.Coord.Distance(u.position, neighbour);
                        if (alt < distance[neighbour])
                        {
                            //queue.Remove(n);
                            distance[neighbour] = alt;
                            previous[neighbour] = u.position;
                            heap.DecreaseDistance(neighbour, alt);
                        }
                    }
                }
            }

            MapGenerator.Coord keyVertex = lastVertex;

            results.path.Add(keyVertex);

            while (previous.TryGetValue(keyVertex, out MapGenerator.Coord parentVertex))
            {
                results.path.Add(parentVertex);
                keyVertex = parentVertex;
            }
            results.path.Reverse();

            return results;
        }

        public PathFindingResults AStarDistance(MapGenerator.Coord start, MapGenerator.Coord dest)
        {
            return AStar(start, dest, MapGenerator.Coord.Distance);
        }

        public PathFindingResults AStarManhattanDistance(MapGenerator.Coord start, MapGenerator.Coord dest)
        {
            return AStar(start, dest, MapGenerator.Coord.ManhattanDistance);
        }

        public PathFindingResults AStar(MapGenerator.Coord start, MapGenerator.Coord dest, Func<MapGenerator.Coord, MapGenerator.Coord, float> heuristic)
        {
            PathFindingResults results = new PathFindingResults();
            Dictionary<MapGenerator.Coord, float> distance = new Dictionary<MapGenerator.Coord, float>();
            Dictionary<MapGenerator.Coord, float> heuristicValue = new Dictionary<MapGenerator.Coord, float>();
            Dictionary<MapGenerator.Coord, MapGenerator.Coord> previous = new Dictionary<MapGenerator.Coord, MapGenerator.Coord>();
            Heap heap = new Heap();


            foreach (MapGenerator.Coord vertex in adjacencyList.Keys)
            {
                distance.Add(vertex, int.MaxValue);
                heuristicValue.Add(vertex, float.PositiveInfinity);
            }

            distance[start] = 0;
            heuristicValue[start] = heuristic(start, dest);
            heap.Insert(start, heuristicValue[start]);

            MapGenerator.Coord lastVertex = start;

            while (heap.Count > 0)
            {
                var u = heap.TakeMin();
                lastVertex = u.position;

                results.visited.Add(u.position); // for visualisation of visited nodes.

                if (u.position == dest)
                {
                    break;
                }

                foreach (MapGenerator.Coord neighbour in adjacencyList[u.position])
                {
                    float alt = u.distance + MapGenerator.Coord.Distance(u.position, neighbour);
                    if (alt < distance[neighbour])
                    {
                        //queue.Remove(n);
                        distance[neighbour] = alt;
                        heuristicValue[neighbour] = alt + heuristic(neighbour, dest);
                        if (!heap.Contains(neighbour))
                        {
                            heap.Insert(neighbour, heuristicValue[neighbour]);
                        }
                        else
                        {
                            heap.DecreaseDistance(neighbour, heuristicValue[neighbour]);
                        }
                        previous[neighbour] = u.position;
                    }
                }
            }

            MapGenerator.Coord keyVertex = lastVertex;

            results.path.Add(keyVertex);

            while (previous.TryGetValue(keyVertex, out MapGenerator.Coord parentVertex))
            {
                results.path.Add(parentVertex);
                keyVertex = parentVertex;
            }
            results.path.Reverse();

            return results;
        }
    }
}

public class PathFindingResults
{
    public List<MapGenerator.Coord> path = new List<MapGenerator.Coord>();
    public List<MapGenerator.Coord> visited = new List<MapGenerator.Coord>();
}

class Heap
{
    Dictionary<MapGenerator.Coord, int> indices = new Dictionary<MapGenerator.Coord, int>();
    List<HeapNode> nodes = new List<HeapNode>();
    int lastNodeIndex = 0;

    public struct HeapNode
    {
        public float distance;
        public MapGenerator.Coord position;

        public HeapNode(float distance, MapGenerator.Coord position)
        {
            this.distance = distance;
            this.position = position;
        }
    }

    public Heap()
    {
        nodes.Add(new HeapNode()); // Dummy node at index 0
    }

    public HeapNode Min
    {
        get
        {
            if (lastNodeIndex == 0)
            {
                throw new Exception("Heap is empty.");
            }
            return nodes[1];
        }
    }

    public int Count
    {
        get
        {
            return lastNodeIndex;
        }
    }

    /*
     * Removes the minimum (root) node and returns it.
     */
    public HeapNode TakeMin()
    {
        if (lastNodeIndex == 0)
        {
            throw new Exception("Heap is empty.");
        }
        HeapNode minNode = Min;
        Swap(1, lastNodeIndex);
        indices.Remove(nodes[lastNodeIndex].position);
        nodes.RemoveAt(lastNodeIndex);
        --lastNodeIndex;
        Heapify(1);
        return minNode;
    }

    public void Insert(MapGenerator.Coord position, float distance)
    {
        if (indices.ContainsKey(position)) { throw new Exception("Vertex already in heap!"); }
        HeapNode node = new HeapNode(distance, position);
        nodes.Add(node);
        ++lastNodeIndex;
        indices[node.position] = lastNodeIndex;
        BubbleUp(lastNodeIndex);
    }

    /*
     * Decreases the distance assigned to a node in the heap.
     * THE NEW VALUE MUST BE LESS THAN THE EXISTING DISTANCE.
     */
    public void DecreaseDistance(MapGenerator.Coord position, float newDistance)
    {
        int index = indices[position];
        if (nodes[index].distance == newDistance) { return; }
        if (nodes[index].distance < newDistance) { throw new Exception("distance must be lower than the existing distance."); }
        nodes[index] = new HeapNode(newDistance, position);
        BubbleUp(index);
    }

    /*
     * Returns true if the heap constains a position.
     */
    public bool Contains(MapGenerator.Coord position)
    {
        return indices.ContainsKey(position);
    }

    void BubbleUp(int index)
    {
        if (index == 1) { return; }
        int currentIndex = index;
        int parentIndex = GetParentIndex(currentIndex);
        while (nodes[parentIndex].distance > nodes[currentIndex].distance)
        {
            Swap(currentIndex, parentIndex);
            currentIndex = parentIndex;
            if (currentIndex == 1) { break; }
            else { parentIndex = GetParentIndex(currentIndex); }
        }
    }

    void Heapify(int root)
    {
        int leftIndex = root * 2;
        int rightIndex = leftIndex + 1;

        int minIndex = root;

        if (leftIndex <= lastNodeIndex && nodes[leftIndex].distance < nodes[minIndex].distance)
        {
            minIndex = leftIndex;
        }
        if (rightIndex <= lastNodeIndex && nodes[rightIndex].distance < nodes[minIndex].distance)
        {
            minIndex = rightIndex;
        }
        if (minIndex != root)
        {
            Swap(minIndex, root);
            Heapify(minIndex);
        }
    }

    void Swap(int indexA, int indexB)
    {
        HeapNode temp = nodes[indexB];
        nodes[indexB] = nodes[indexA];
        nodes[indexA] = temp;
        // Update indices
        indices[nodes[indexA].position] = indexA;
        indices[nodes[indexB].position] = indexB;
    }

    int GetParentIndex(int nodeIndex)
    {
        return (int)Math.Floor(nodeIndex / 2.0);
    }

    //public override string ToString()
    //{
    //    StringBuilder builder = new StringBuilder();
    //    builder.Append("MinHeap: ");
    //    for (int i = 1; i < nodes.Count; i++)
    //    {
    //        builder.Append(nodes[i].distance);
    //        if (i < nodes.Count - 1)
    //        {
    //            builder.Append(", ");
    //        }
    //    }
    //    return builder.ToString();
    //}
}



