﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputHandler : MonoBehaviour
{
    [SerializeField] MapGenerator mapGenerator;
    [SerializeField] PathFinding pathFinding;
    [SerializeField] float visitedTilesPerSecond;
    [SerializeField] float pathTilesPerSecond;
    [SerializeField] Color visitedTileColor;
    [SerializeField] Color pathColor;
    [SerializeField] Color selectionColor;

    int algorithmSelection = 1;

    Step step;


    private void Awake()
    {
        BuildMap();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out RaycastHit hit, 100) && GetComponent<DrawComponent>() == null)
            {
                Renderer cubeRenderer = hit.transform.gameObject.GetComponent<Renderer>();
                if (hit.transform.gameObject.tag == "White")
                {
                    cubeRenderer.material.SetColor("_Color", selectionColor);

                    int x = hit.transform.gameObject.GetComponent<Cube>().x;
                    int y = hit.transform.gameObject.GetComponent<Cube>().y;
                    
                    TryFindPath(new MapGenerator.Coord(x, y));
                    // TODO:
                    // Only select two points at a time. Point 1 and Point 2 are red, old points return to being white (only white squares can be selected).
                    // Create a Path formed of two coords (point 1 and point 2)
                    // Run Path finding algorithm on Path
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            BuildMap();
        }

        if (Input.GetKeyDown(KeyCode.Alpha1)){
            algorithmSelection = 1;
            
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            algorithmSelection = 2;
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            algorithmSelection = 3;
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            algorithmSelection = 4;
        }
        if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            algorithmSelection = 5;
        }

        if (Input.GetKey("escape"))
        {
            Application.Quit();
        }
    }

    void BuildMap()
    {
        DrawComponent[] components = GetComponents<DrawComponent>();
        foreach (DrawComponent component in components){
            DestroyImmediate(component);
        }
        mapGenerator.GenerateMap();
        pathFinding.graph.BuildGraph(mapGenerator);
        mapGenerator.Draw();
    }

    /*
     * Tries to append a new coordinate to the step, runs the selected path finding algorithm if step is set.
     */
    public void TryFindPath(MapGenerator.Coord coord)
    {
        if (IsCoordEmpty(step.start))
        {
            step.start = coord;
        }
        else
        {
            step.end = coord;
            
            mapGenerator.Draw();

            DrawComponent component = gameObject.AddComponent<DrawComponent>();
            switch (algorithmSelection)
            {
                case 1:
                    component.results = pathFinding.graph.DepthFirst(step.start, step.end);
                    break;
                case 2:
                    component.results = pathFinding.graph.BreadthFirst(step.start, step.end);
                    break;
                case 3:
                    component.results = pathFinding.graph.Dijkstra(step.start, step.end);
                    break;
                case 4:
                    component.results = pathFinding.graph.AStarDistance(step.start, step.end);
                    break;
                case 5:
                    component.results = pathFinding.graph.AStarManhattanDistance(step.start, step.end);
                    break;
                default: // shouldn't run anyway
                    component.results = pathFinding.graph.DepthFirst(step.start, step.end);
                    break;

            }
            component.map = mapGenerator;
            component.visitedTilesPerSecond = visitedTilesPerSecond;
            component.pathTilesPerSecond = pathTilesPerSecond;
            component.pathColor = pathColor;
            component.visitedTileColor = visitedTileColor;

            step = new Step(new MapGenerator.Coord(0, 0), new MapGenerator.Coord(0, 0)); // reset step.
            // reset map
        }
    }

    /*
     * checks if a coordinate in the path is set to 0
     */
    bool IsCoordEmpty(MapGenerator.Coord coord)
    {
        return coord.tileX == 0 && coord.tileY == 0;
    }

    /*
     * if either Coord in the path is empty returns true
     */
    public bool PathContainsEmpty(Step step)
    {
        return IsCoordEmpty(step.start) || IsCoordEmpty(step.end);
    }

    public struct Step
    {
        public MapGenerator.Coord start;
        public MapGenerator.Coord end;

        public Step(MapGenerator.Coord start, MapGenerator.Coord end)
        {
            this.start = start;
            this.end = end;
        }
    }
}
