﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator : MonoBehaviour
{
    public int width; // The width of the map.
    public int height; // The height of the map.
    public int smoothSteps; // number of smoothing steps to apply. (default: 5).
    public string seed; // The seed to generate the map by;
    public bool useRandomSeed; // true: generate a random seed. false: use the currently supplied seed.
    public Color tileColor;

    [Range(0, 100)]
    public int fillPercent; // the amount of space that will be impassable wall prior to smoothing.
    [Range(1, 8)]
    public int adjacentWallThreshold; // number of walls (1s) in adjacent cells (default: 4)

    public int[,] map; // grid of integers, 0 = walkable space, 1 = impassable wall. Read Map.
    public GameObject[,] cubes;

    int[,] tempMap; // grid of integers, 0 = walkable space, 1 = impassable wall. Write Map.

    [SerializeField] private GameObject whitePrefab;
    [SerializeField] private GameObject blackPrefab;
    [SerializeField] private Vector3 prefabDimensions = new Vector3(1.0f, 1.0f, 0.0f);


    public void GenerateMap()
    {
        map = new int[width, height];
        tempMap = new int[width, height];
        FillMap();

        for (int i = 0; i < smoothSteps; i++)
        {
            SmoothMap();
        }

        RemoveInvalidRegions();

        // Set up and draw cubes

        cubes = new GameObject[width, height];
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        for (int i = 0; i < width; ++i)
        {
            for (int j = 0; j < height; ++j)
            {
                var offset = transform.position - Vector3.Scale(prefabDimensions, new Vector3(width, height, 0.0f) / 2.0f);
                var position = offset + Vector3.Scale(prefabDimensions, new Vector3(i, j, 0.0f));
                GameObject clone;
                if (map[i, j] == 0)
                {
                    clone = Instantiate(whitePrefab, position, Quaternion.identity, transform);
                    clone.GetComponent<Cube>().x = i;
                    clone.GetComponent<Cube>().y = j;
                    cubes[i, j] = clone;
                }
            }
        }
    }


    public void Draw()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if(map[x, y] == 0)
                {
                    cubes[x, y].GetComponent<Renderer>().material.SetColor("_Color", tileColor);
                }
            }
        }
    }

    void RemoveInvalidRegions()
    {
        List<List<Coord>> wallRegions = GetRegions(1);
        int wallRegionThresholdSize = 50;

        foreach (List<Coord> wallRegion in wallRegions)
        {
            if (wallRegion.Count < wallRegionThresholdSize)
            {
                foreach (Coord tile in wallRegion)
                {
                    map[tile.tileX, tile.tileY] = 0;
                }
            }
        }

        List<List<Coord>> roomRegions = GetRegions(0);
        int roomRegionThresholdSize = 50;
        List<Room> validRooms = new List<Room>();

        foreach (List<Coord> roomRegion in roomRegions)
        {
            if (roomRegion.Count < roomRegionThresholdSize)
            {
                foreach (Coord tile in roomRegion)
                {
                    map[tile.tileX, tile.tileY] = 1;
                }
            }
            else
            {
                validRooms.Add(new Room(roomRegion, map));
            }
        }
        validRooms.Sort();
        validRooms[0].isAccessibleFromMainRoom = true;
        validRooms[0].isMainRoom = true;
        ConnectClosestRooms(validRooms);
    }

    void ConnectClosestRooms(List<Room> allRooms, bool forceAccessibleFromMainRoom = false)
    {
        List<Room> roomListA = new List<Room>();
        List<Room> roomListB = new List<Room>();

        if (forceAccessibleFromMainRoom)
        {
            foreach (Room room in allRooms)
            {
                if (room.isAccessibleFromMainRoom)
                {
                    roomListB.Add(room);
                }
                else
                {
                    roomListA.Add(room);
                }
            }
        }
        else
        {
            roomListA = allRooms;
            roomListB = allRooms;
        }

        int shortestDistance = 0;
        Coord bestTileA = new Coord();
        Coord bestTileB = new Coord();
        Room bestRoomA = new Room();
        Room bestRoomB = new Room();
        bool possibleConnectionFound = false;

        foreach (Room roomA in roomListA)
        {
            if (!forceAccessibleFromMainRoom)
            {
                possibleConnectionFound = false;
                if(roomA.connectedRooms.Count > 0)
                {
                    continue;
                }
            }

            foreach (Room roomB in roomListB)
            {
                if (roomA == roomB || roomA.IsConnected(roomB)) // if roomA and roomB are the same room, no path is possible.
                {
                    continue;
                }

                for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; tileIndexA++)
                {
                    for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; tileIndexB++)
                    {
                        Coord tileA = roomA.edgeTiles[tileIndexA];
                        Coord tileB = roomB.edgeTiles[tileIndexB];
                        //int distanceBetweenRooms = (int)(Mathf.Pow(tileA.tileX - tileB.tileX, 2) + Mathf.Pow(tileA.tileY - tileB.tileY, 2));

                        var a = tileA.tileX - tileB.tileX;
                        var b = tileA.tileY - tileB.tileY;
                        int distanceBetweenRooms = (int)(a * a + b * b);

                        if (distanceBetweenRooms < shortestDistance || !possibleConnectionFound)
                        {
                            shortestDistance = distanceBetweenRooms;
                            possibleConnectionFound = true;
                            bestTileA = tileA;
                            bestTileB = tileB;
                            bestRoomA = roomA;
                            bestRoomB = roomB;
                        }
                    }
                }
            }

            if (possibleConnectionFound && !forceAccessibleFromMainRoom)
            {
                CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            }
        }

        if (possibleConnectionFound && forceAccessibleFromMainRoom)
        {
            CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
            ConnectClosestRooms(allRooms, true);
        }

        if (!forceAccessibleFromMainRoom)
        {
            ConnectClosestRooms(allRooms, true);
        }
    }

    void CreatePassage(Room roomA, Room roomB, Coord tileA, Coord tileB)
    {
        Room.ConnectedRooms(roomA, roomB);
        List<Coord> line = GetLine(tileA, tileB);
        foreach(Coord c in line)
        {
            DrawCircle(c, 3);
        }
    }

    void DrawCircle(Coord c, int r)
    {
        for(int x = -r; x <= r; x++)
        {
            for(int y = -r; y <= r; y++)
            {
                if(x * x + y * y <= r * r)
                {
                    int drawX = c.tileX + x;
                    int drawY = c.tileY + y;
                    if (IsInMapRange(drawX, drawY))
                    {
                        map[drawX, drawY] = 0;
                    }
                }
            }
        }
    }

    List<Coord> GetLine(Coord from, Coord to)
    {
        List<Coord> line = new List<Coord>();

        int x = from.tileX;
        int y = from.tileY;

        int dx = to.tileX - from.tileX;
        int dy = to.tileY - from.tileY;

        bool inverted = false;
        int step = Math.Sign(dx);
        int gradientStep = Math.Sign(dy);

        int longest = Math.Abs(dx);
        int shortest = Math.Abs(dy);

        if(longest < shortest)
        {
            inverted = true;
            longest = Math.Abs(dy);
            shortest = Math.Abs(dx);

            step = Math.Sign(dy);
            gradientStep = Math.Sign(dx);
        }

        int gradientAccumulation = longest / 2;
        for(int i = 0; i < longest; i++)
        {
            line.Add(new Coord(x, y));

            if (inverted)
            {
                y += step;
            }
            else
            {
                x += step;
            }

            gradientAccumulation += shortest;
            if(gradientAccumulation >= longest)
            {
                if (inverted)
                {
                    x += gradientStep;
                }
                else
                {
                    y += gradientStep;
                }
                gradientAccumulation -= longest;
            }
        }
        return line;
    }

    /*
     * Returns a list of Regions
     */
    List<List<Coord>> GetRegions(int tileType)
    {
        List<List<Coord>> regions = new List<List<Coord>>();
        int[,] mapFlags = new int[width, height];

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                {
                    List<Coord> newRegion = GetRegionTiles(x, y);
                    regions.Add(newRegion);

                    foreach (Coord tile in newRegion)
                    {
                        mapFlags[tile.tileX, tile.tileY] = 1;
                    }
                }
            }
        }
        return regions;
    }

    /*
     * Returns a list of tiles belonging to a particular region.
     */
    List<Coord> GetRegionTiles(int startX, int startY)
    {
        List<Coord> tiles = new List<Coord>();
        int[,] mapFlags = new int[width, height];
        int tileType = map[startX, startY];

        Queue<Coord> queue = new Queue<Coord>();
        queue.Enqueue(new Coord(startX, startY));
        mapFlags[startX, startY] = 1;

        while (queue.Count > 0)
        {
            Coord tile = queue.Dequeue();
            tiles.Add(tile);

            for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
            {
                for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
                {
                    if (IsInMapRange(x, y) && (y == tile.tileY || x == tile.tileX))
                    {
                        if (mapFlags[x, y] == 0 && map[x, y] == tileType)
                        {
                            mapFlags[x, y] = 1;
                            queue.Enqueue(new Coord(x, y));
                        }
                    }
                }
            }
        }
        return tiles;
    }

    public bool IsInMapRange(int x, int y)
    {
        return x >= 0 && x < width && y >= 0 && y < height;
    }

    /*
     * Pseudo randomly fills a map of integers with 0s and 1s
     */
    void FillMap()
    {
        if (useRandomSeed)
        {
            seed = DateTime.Now.Ticks.ToString();
        }

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if (x == 0 || x == width - 1 || y == 0 || y == height - 1)
                {
                    map[x, y] = 1; // if the node is at the edge of the map it is a wall.
                }
                else
                {
                    map[x, y] = (pseudoRandom.Next(0, 100) < fillPercent) ? 1 : 0; // sets the value of each node to 1 or 0
                }
            }
        }
    }

    /*
     * Copies the contents of a 2D integer array to a second 2D integer array.
     * (This is used to eliminate a directional bias in the cellular automata)
     */
    void CopyMap(ref int[,] fromMap, ref int[,] toMap)
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                toMap[x, y] = fromMap[x, y];
            }
        }
    }

    /*
     * Smooths map contents to create a cave-like map of 0s and 1s
     */
    void SmoothMap()
    {
        CopyMap(ref map, ref tempMap); // copy the contents of map to tempMap.
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int adjacentWalls = GetAdjacentWallCount(x, y); // get the number of adjacent 1s to each cell.
                if (adjacentWalls > adjacentWallThreshold)
                {
                    tempMap[x, y] = 1; // if there are more than FOUR 1s adjacent to the cell, the cell also becomes a 1.
                }
                else if (adjacentWalls < adjacentWallThreshold)
                {
                    tempMap[x, y] = 0; // if there are less than FOUR 1s adjacent to the cell, the cell becomes a 0.
                }
            }
        }
        CopyMap(ref tempMap, ref map); // copy the contents of tempMap to map.
    }

    /*
     * Looks at a 3x3 area around a given coordinate (x, y) and returns the number of walls (1s) surrounding it.
     */
    int GetAdjacentWallCount(int x, int y)
    {
        int wallCount = 0;
        for (int nx = x - 1; nx <= x + 1; nx++)
        {
            for (int ny = y - 1; ny <= y + 1; ny++)
            {
                if (IsInMapRange(nx, ny))
                {
                    if (nx != x || ny != y)
                    {
                        wallCount += map[nx, ny]; // if not looking at the coordinate in question adds the value of coordinate nx to wallcount.
                    }
                }
                else
                {
                    wallCount++; // Encourage the growth of walls near the edges of the map.
                }
            }
        }
        return wallCount;
    }

    public struct Coord
    {
        public int tileX;
        public int tileY;

        public Coord(int x, int y)
        {
            tileX = x;
            tileY = y;
        }
        public static bool operator ==(Coord lhs, Coord rhs)
        {
            return lhs.tileX == rhs.tileX && lhs.tileY == rhs.tileY;
        }

        public static bool operator !=(Coord lhs, Coord rhs)
        {
            return !(lhs == rhs);
        }

        public static float Distance(Coord coordA, Coord coordB)
        {
            return Mathf.Sqrt((coordB.tileX - coordA.tileX) * (coordB.tileX - coordA.tileX) + (coordB.tileY - coordA.tileY) * (coordB.tileY - coordA.tileY));
        }

        public static float ManhattanDistance(Coord coordA, Coord coordB)
        {
            return Mathf.Abs(coordB.tileX - coordA.tileX) + Mathf.Abs(coordB.tileY - coordA.tileY);
        }
    }

    class Room : IComparable<Room>
    {
        public List<Coord> tiles;
        public List<Coord> edgeTiles;
        public List<Room> connectedRooms;
        public int roomSize;
        public bool isAccessibleFromMainRoom;
        public bool isMainRoom;

        public Room()
        {

        }

        public Room(List<Coord> roomTiles, int[,] map)
        {
            tiles = roomTiles;
            roomSize = tiles.Count;
            connectedRooms = new List<Room>();

            edgeTiles = new List<Coord>();
            foreach (Coord tile in tiles)
            {
                for (int x = tile.tileX - 1; x <= tile.tileX + 1; x++)
                {
                    for (int y = tile.tileY - 1; y <= tile.tileY + 1; y++)
                    {
                        if (x == tile.tileX || y == tile.tileY)
                        {
                            if (map[x, y] == 1)
                            {
                                edgeTiles.Add(tile);
                            }
                        }
                    }
                }
            }
        }

        public void SetAccessibleFromMainRoom()
        {
            if (!isAccessibleFromMainRoom)
            {
                isAccessibleFromMainRoom = true;
                foreach (Room connectedRoom in connectedRooms)
                {
                    connectedRoom.SetAccessibleFromMainRoom();
                }
            }
        }

        public static void ConnectedRooms(Room roomA, Room roomB)
        {
            if (roomA.isAccessibleFromMainRoom)
            {
                roomB.SetAccessibleFromMainRoom();
            }
            else if (roomB.isAccessibleFromMainRoom)
            {
                roomA.SetAccessibleFromMainRoom();
            }
            roomA.connectedRooms.Add(roomB);
            roomB.connectedRooms.Add(roomA);
        }

        public bool IsConnected(Room otherRoom)
        {
            return connectedRooms.Contains(otherRoom);
        }

        public int CompareTo(Room otherRoom)
        {
            return otherRoom.roomSize.CompareTo(roomSize);
        }
    }
}