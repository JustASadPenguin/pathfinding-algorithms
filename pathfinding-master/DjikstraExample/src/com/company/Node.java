package com.company;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Node {

    private String name; // name of the node.
    private List<Node> shortestPath = new LinkedList<>(); // the shortest path to this node from the start point.
    private Integer distance = Integer.MAX_VALUE; // the distance variable of this Node, set as close to infinity as possible when initialised.
    Map<Node, Integer> adjacentNodes = new HashMap<>(); // Nodes adjacent to this Node.

    /**
     * adds an adjacent Node to the adjacentNodes Map.
     *
     * @param destination the adjacent Node
     * @param distance    the Weight of the vertex between this Node and the adjacent Node
     */
    public void addDestination(Node destination, int distance) {
        adjacentNodes.put(destination, distance); // add the adjacent Node to the Map.
    }

    public Node(String name) {
        this.name = name;
    }

    /**
     * sets the distance variable of this Node to a new value.
     *
     * @param distance the new distance.
     */
    public void setDistance(int distance) {
        this.distance = distance;
    }

    /**
     * @return the distance variable of this Node.
     */
    public int getDistance() {
        return distance;
    }

    /**
     * @return the shortestPath variable of this Node.
     */
    public List<Node> getShortestPath() {
        return shortestPath;
    }

    /**
     * Sets the shortestPath to a new List of Nodes
     *
     * @param shortestPath LinkedList of Nodes indicating a new shortest path.
     */
    public void setShortestPath(List<Node> shortestPath) {
        this.shortestPath = shortestPath;
    }

    /**
     * @return the HashMap of adjacent Nodes
     */
    public Map<Node, Integer> getAdjacentNodes() {
        return adjacentNodes;
    }

    /**
     * @return the name of the Node
     */
    public String getName() {
        return name;
    }
}
