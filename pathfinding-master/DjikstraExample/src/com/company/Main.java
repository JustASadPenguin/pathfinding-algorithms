package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // Initialise nodes for graph
        Node nodeS = new Node("S");
        Node nodeA = new Node("A");
        Node nodeB = new Node("B");
        Node nodeC = new Node("C");
        Node nodeD = new Node("D");
        Node nodeE = new Node("E");
        Node nodeF = new Node("F");
        Node nodeG = new Node("G");
        Node nodeH = new Node("H");
        Node nodeI = new Node("I");
        Node nodeJ = new Node("J");
        Node nodeK = new Node("K");
        Node nodeL = new Node("L");

        // Connecting adjacent nodes
        // Adjacent nodes for nodeS
        nodeS.addDestination(nodeA, 7);
        nodeS.addDestination(nodeB, 2);
        nodeS.addDestination(nodeC, 3);

        // Adjacent nodes for nodeA
        nodeA.addDestination(nodeS, 7);
        nodeA.addDestination(nodeB, 3);
        nodeA.addDestination(nodeD, 4);

        // Adjacent nodes for nodeB
        nodeB.addDestination(nodeS, 2);
        nodeB.addDestination(nodeA, 3);
        nodeB.addDestination(nodeD, 4);
        nodeB.addDestination(nodeH, 1);

        // Adjacent nodes for nodeC
        nodeC.addDestination(nodeS, 3);
        nodeC.addDestination(nodeL, 2);

        // Adjacent nodes for nodeD
        nodeD.addDestination(nodeA, 4);
        nodeD.addDestination(nodeB, 4);
        nodeD.addDestination(nodeF, 5);

        // Adjacent nodes for nodeE
        nodeE.addDestination(nodeG, 2);
        nodeE.addDestination(nodeK, 5);

        // Adjacent nodes for nodeF
        nodeF.addDestination(nodeD, 5);
        nodeF.addDestination(nodeH, 3);

        // Adjacent nodes for nodeG
        nodeG.addDestination(nodeH, 2);
        nodeG.addDestination(nodeE, 2);

        // Adjacent nodes for nodeH
        nodeH.addDestination(nodeG, 2);
        nodeH.addDestination(nodeF, 3);
        nodeH.addDestination(nodeB, 1);

        // Adjacent nodes for nodeI
        nodeI.addDestination(nodeL, 4);
        nodeI.addDestination(nodeJ, 6);
        nodeI.addDestination(nodeK, 4);

        // Adjacent nodes for nodeJ
        nodeJ.addDestination(nodeI, 6);
        nodeJ.addDestination(nodeK, 4);
        nodeJ.addDestination(nodeL, 4);

        // Adjacent nodes for nodeK
        nodeK.addDestination(nodeE, 5);
        nodeK.addDestination(nodeI, 4);
        nodeK.addDestination(nodeL, 4);

        // Adjacent nodes for nodeL
        nodeL.addDestination(nodeC, 2);
        nodeL.addDestination(nodeI, 4);
        nodeL.addDestination(nodeJ, 4);

        // Initialise a blank graph
        Graph graph = new Graph();

        // Add nodes to graph
        graph.addNode(nodeS);
        graph.addNode(nodeA);
        graph.addNode(nodeB);
        graph.addNode(nodeC);
        graph.addNode(nodeD);
        graph.addNode(nodeE);
        graph.addNode(nodeF);
        graph.addNode(nodeG);
        graph.addNode(nodeH);
        graph.addNode(nodeI);
        graph.addNode(nodeJ);
        graph.addNode(nodeK);
        graph.addNode(nodeL);

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the name of a node you want to find a path from (A-L, S): ");
        Node inputNode;
        String input = sc.next().toLowerCase();

        switch (input) {
            case "nodes":
            case "s":
                inputNode = nodeS;
                break;
            case "nodea":
            case "a":
                inputNode = nodeA;
                break;
            case "nodeb":
            case "b":
                inputNode = nodeB;
                break;
            case "nodec":
            case "c":
                inputNode = nodeC;
                break;
            case "noded":
            case "d":
                inputNode = nodeD;
                break;
            case "nodee":
            case "e":
                inputNode = nodeE;
                break;
            case "nodef":
            case "f":
                inputNode = nodeF;
                break;
            case "nodeg":
            case "g":
                inputNode = nodeG;
                break;
            case "nodeh":
            case "h":
                inputNode = nodeH;
                break;
            case "nodei":
            case "i":
                inputNode = nodeI;
                break;
            case "nodej":
            case "j":
                inputNode = nodeJ;
                break;
            case "nodek":
            case "k":
                inputNode = nodeK;
                break;
            case "nodel":
            case "l":
                inputNode = nodeL;
                break;
            default:
                System.out.println("Sorry! That appears to be an invalid node, here are the shortest paths from node S");
                inputNode = nodeS;
                break;
        }


        // Calculate the shortest path on the graph from nodeS to all other nodes
        Dijkstra.calculateShortestPath(inputNode);

        for (int i = 0; i < graph.getNodes().size(); i++) {
            for (int j = 0; j < graph.getNodes().get(i).getShortestPath().size(); j++) {
                // for each node in the listed path print the name (Starts at S)
                System.out.format("%s > ", graph.getNodes().get(i).getShortestPath().get(j).getName());
            }
            // print out the node this path is for and the distance (accumulative weight) of the path.
            System.out.format("%s, distance: %d\n", graph.getNodes().get(i).getName(), graph.getNodes().get(i).getDistance());
        }
    }
}
