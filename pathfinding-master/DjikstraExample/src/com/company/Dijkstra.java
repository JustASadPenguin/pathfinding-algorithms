package com.company;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

public class Dijkstra {

    /**
     * goes through all Nodes following the shortest path from the source Node to all other Nodes.
     *
     * @param source the source Node (starting Node).
     */
    public static void calculateShortestPath(Node source) {
        source.setDistance(0); // distance to starting Node will always be 0

        Set<Node> settledNodes = new HashSet<>(); // Set of Nodes that are settled
        Set<Node> unsettledNodes = new HashSet<>(); // Set of Nodes that are unsettled

        unsettledNodes.add(source); // add the starting node to the list of unsettled nodes

        while (unsettledNodes.size() != 0) {
            Node currentNode = getClosestNode(unsettledNodes); // call getLowestDistance on all nodes in the unsettled nodes set.
            unsettledNodes.remove(currentNode); // remove current node from the set to avoid going backwards.
            for (Map.Entry<Node, Integer> adjacencyPair : currentNode.getAdjacentNodes().entrySet()) { // for each entry(node, distance) in the map of adjacent nodes.
                Node adjacentNode = adjacencyPair.getKey();
                Integer edgeWeight = adjacencyPair.getValue();
                if (!settledNodes.contains(adjacentNode)) {
                    calculateMinDistance(adjacentNode, edgeWeight, currentNode); // if the set of settled Nodes does not contain the adjacent Node calculate minimum distance
                    unsettledNodes.add(adjacentNode); // add the adjacent node to the set of unsettled nodes
                }
            }
            settledNodes.add(currentNode); // add the current node to the set of settled nodes
        }
    }

    /**
     * @param unsettledNodes HashSet of nodes that havent been settled by the calculateShortestPath() method
     * @return the closest node to the unsettled Node
     */
    private static Node getClosestNode(Set<Node> unsettledNodes) {
        Node closestNode = null;
        int lowestDistance = Integer.MAX_VALUE;
        for (Node node : unsettledNodes) {
            int nodeDistance = node.getDistance();
            if (nodeDistance < lowestDistance) {
                lowestDistance = nodeDistance;
                closestNode = node;
            }
        }
        return closestNode;
    }

    /**
     * Calculates the shortest path to a node from the source node.
     *
     * @param evaluationNode the node to find the shortest path for.
     * @param edgeWeight     the weight (distance) between one node and another.
     * @param sourceNode     the source node.
     */
    private static void calculateMinDistance(Node evaluationNode, Integer edgeWeight, Node sourceNode) {
        Integer sourceDistance = sourceNode.getDistance();
        if (sourceDistance + edgeWeight < evaluationNode.getDistance()) {
            evaluationNode.setDistance(sourceDistance + edgeWeight);
            LinkedList<Node> shortestPath = new LinkedList<>(sourceNode.getShortestPath());
            shortestPath.add(sourceNode);
            evaluationNode.setShortestPath(shortestPath);
        }
    }
}
