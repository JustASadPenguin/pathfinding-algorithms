package com.company;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Graph {

    private List<Node> nodes = new LinkedList<>();

    /**
     * Adds a Node to the graph.
     *
     * @param node the node to be added.
     */
    public void addNode(Node node) {
        nodes.add(node);
    }

    public List<Node> getNodes() {
        return nodes;
    }
}